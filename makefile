GCC = g++
CPPFLAGS = -std=c++11 -O3
CPPSRC = $(wildcard *.cpp)
CPPOBJ = $(CPPSRC:%.cpp=%.o)
CPPEXE = $(CPPSRC:%.cpp=%)

DMD = dmd
RDMD = rdmd
DFLAGS = -O -release -inline -noboundscheck
TESTDFLAGS = -unittest -main
DSRC = $(wildcard *.d)
DOBJ = $(DSRC:%.d=%.o)
DEXE = $(DSRC:%.d=%)

.PHONY: all clean

all: $(CPPEXE) $(DEXE)

CGALFLAGS = -lCGAL -lgmp -lmpfr -lm 
OS := $(shell uname)	

ifeq ($(OS),Darwin)
	CGALFLAGS += -lboost_thread-mt
else
	CGALFLAGS += -lboost_thread
endif


compute_filtration: compute_filtration.cpp
	echo $(OS)
	echo $(CGALFLAGS)
	$(GCC) $(CPPFLAGS) $^ $(CGALFLAGS) -o $@

compute_filtration_test: compute_filtration_test.cpp
	echo $(OS)
	echo $(CGALFLAGS)
	$(GCC) $(CPPFLAGS) $^ $(CGALFLAGS) -o $@

filtration_xyz_to_binID: filtration_xyz_to_binID.d
	$(DMD) $(DMDFLAGS) $^

calc_diameter: calc_diameter.d
	$(DMD) $(DMDFLAGS) $^

clean:
	rm *~ $(CPPOBJ) $(CPPEXE) $(DOBJ) $(DEXE)

