#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <stdlib.h>
#include <math.h>


// types in CGAL package; Delaunay_triangulation_3
typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_3<K> Dt3D;
typedef Dt3D::Point Point;
typedef Dt3D::Vertex Vertex;
typedef Dt3D::Edge Edge;
typedef Dt3D::Facet Facet;
typedef Dt3D::Cell Cell;
typedef Dt3D::Finite_vertices_iterator Vertex_it;
typedef Dt3D::Finite_edges_iterator Edge_it;
typedef Dt3D::Finite_facets_iterator Facet_it;
typedef Dt3D::Finite_cells_iterator Cell_it;
typedef K::Vector_3 Vector;


class Simplex
{
  friend std::ostream& operator<<(std::ostream& strm, const Simplex& s);
  
private:
  std::vector<Point> points_;
  double fVal_;
  bool validFlag_;

  bool isValidAplphaSimplex(Point p0, Point p1, Point p2) {
    if(CGAL::angle(p0, p1, p2) == CGAL::OBTUSE
       || CGAL::angle(p2, p0, p1) == CGAL::OBTUSE
       || CGAL::angle(p1, p2, p0) == CGAL::OBTUSE)
      return false;
    else
      return true;
  }

  
  bool isValidAplphaSimplex(Point p0, Point p1, Point p2, Point p3) {
    //the normal vectors of 4 facets
    Vector v01(p0, p1);
    Vector v02(p0, p2);
    Vector v03(p0, p3);
    Vector v12(p1, p2);
    Vector v13(p1, p3);
    Vector n012 = CGAL::cross_product(v01, v02);
    Vector n013 = CGAL::cross_product(v01, v03);
    Vector n023 = CGAL::cross_product(v02, v03);
    Vector n123 = CGAL::cross_product(v12, v13);

    //the centroids of 4 facets
    Point c012 = CGAL::centroid(p0, p1, p2);
    Point c013 = CGAL::centroid(p0, p1, p3);
    Point c023 = CGAL::centroid(p0, p2, p3);
    Point c123 = CGAL::centroid(p1, p2, p3);

    //the centroid of the tetrahedron
    Point c0123 = CGAL::centroid(p0, p1, p2, p3);
    
    //the circumcenter of the tetrahedron
    Point circumcenter = CGAL::circumcenter(p0, p1, p2, p3);

    /*checking if the centroid and the circumcenter of the tetrahedron are 
     *in the same side of the plane including a facet by using the fact
     *that the centroid of the tetrahedron is interior.
     */
    //about facet p0 p1 p2
    bool exteriorFlag3 = ((circumcenter - c012) * n012) * ((c0123 - c012) * n012) < 0;
    //about facet p0 p1 p3
    bool exteriorFlag2 = ((circumcenter - c013) * n013) * ((c0123 - c013) * n013) < 0;
    //about facet p0 p2 p3
    bool exteriorFlag1 = ((circumcenter - c023) * n023) * ((c0123 - c023) * n023) < 0;
    //about facet p1 p2 p3
    bool exteriorFlag0 = ((circumcenter - c123) * n123) * ((c0123 - c123) * n123) < 0;
    
    if((exteriorFlag1 & exteriorFlag2 & exteriorFlag3) ||
       (exteriorFlag0 & exteriorFlag2 & exteriorFlag3) ||
       (exteriorFlag0 & exteriorFlag1 & exteriorFlag3) ||
       (exteriorFlag0 & exteriorFlag1 & exteriorFlag2)) {
      std::cerr << "ERROR: the circumcenter is exterior three or more planes."
                << std::endl;
      exit(1);
    }
    else if(exteriorFlag0 || exteriorFlag1 || exteriorFlag2 || exteriorFlag3
            || !isValidAplphaSimplex(p0, p1, p2)
            || !isValidAplphaSimplex(p0, p1, p3)
            || !isValidAplphaSimplex(p0, p2, p3)
            || !isValidAplphaSimplex(p1, p2, p3))
      return false;
    else
      return true;
  }
    
  
public:
  Simplex(const Vertex& v)
  {
    points_.push_back(v.point());
    fVal_ = 0;
    validFlag_ = true;
  }
  
  Simplex(const Edge& e)
  {
    points_.push_back(e.first->vertex(e.second)->point());
    points_.push_back(e.first->vertex(e.third)->point());
    fVal_ = sqrt(CGAL::to_double
                 (CGAL::squared_radius(points_[0], points_[1])));
    validFlag_ = true;
  }
  
  Simplex(const Facet& f)
  {
    for(auto i=0; i<4; i++)
      if(i != f.second)
        points_.push_back(f.first->vertex(i)->point());

    fVal_ = sqrt(CGAL::to_double
                 (CGAL::squared_radius(points_[0], points_[1], points_[2])));
    validFlag_ = isValidAplphaSimplex(points_[0], points_[1], points_[2]);
  }
  
  Simplex(const Cell& c)
  {
    for(auto i=0; i<4; i++)
      points_.push_back(c.vertex(i)->point());

    fVal_ = sqrt(CGAL::to_double
                 (CGAL::squared_radius(points_[0], points_[1],
                                       points_[2], points_[3])));
    validFlag_ = isValidAplphaSimplex(points_[0], points_[1],
                                      points_[2], points_[3]);
  }

  double filtrationValue() const { return fVal_; }
  ulong dim() const { return points_.size() - 1; }
  bool isValid() const { return validFlag_; }
  
  bool operator<(const Simplex& right) const
  {
    return fVal_ == right.filtrationValue()
      ? points_.size() - 1 < right.dim() : fVal_ < right.filtrationValue();
  }
};


std::ostream& operator<<(std::ostream& strm, const Simplex& s) {
  for(const auto& p: s.points_)
    strm << p[0] << ',' << p[1] << ',' << p[2] << '\t';
  return strm << s.filtrationValue();
}

  
class Filtration
{
private:
  std::vector<Simplex> simplices_;
  
public:
  Filtration(const Dt3D& dt)
  {
    // list up simplices with alpha values from the Delaunay diagram
    for(Cell_it it=dt.finite_cells_begin(); it!=dt.finite_cells_end(); ++it)
      simplices_.push_back(Simplex(*it));    

    for(Facet_it it=dt.finite_facets_begin(); it!=dt.finite_facets_end(); ++it)
      simplices_.push_back(Simplex(*it));
 
    for(Edge_it it=dt.finite_edges_begin(); it!=dt.finite_edges_end(); ++it)
      simplices_.push_back(Simplex(*it));
    
    for(Vertex_it it=dt.finite_vertices_begin(); it!=dt.finite_vertices_end(); ++it)
      simplices_.push_back(Simplex(*it));
    
    // make filtration from these simplices
    std::sort(simplices_.begin(), simplices_.end());
  }
    
  std::vector<Simplex>::iterator begin() { return simplices_.begin(); }
  std::vector<Simplex>::iterator end() { return simplices_.end(); }
};


int main(int argc, char** argv)
{
  auto pointsFile = argv[optind++];
  auto outFile = argv[optind++];
  
  std::ifstream fin(pointsFile);
  std::vector<Point> points;
  Point p;
  while(fin >> p) points.push_back(p);

  Dt3D dt(points.begin(), points.end()); //Delaunay trianglation
  Filtration filtration(dt); //make filtration from the Delaunay diagram

  std::ofstream fout(outFile);
  int i=0;
  for(auto it=filtration.begin(); it!=filtration.end(); ++it) {
    if(it->isValid())
      fout << *it << "\n";
  }
      
  return 0;
}
