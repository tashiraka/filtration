#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>


// types in CGAL package; Delaunay_triangulation_3
typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_3<K> Dt3D;
typedef Dt3D::Point Point;
typedef Dt3D::Vertex Vertex;
typedef Dt3D::Edge Edge;
typedef Dt3D::Facet Facet;
typedef Dt3D::Cell Cell;
typedef Dt3D::Finite_vertices_iterator Vertex_it;
typedef Dt3D::Finite_edges_iterator Edge_it;
typedef Dt3D::Finite_facets_iterator Facet_it;
typedef Dt3D::Finite_cells_iterator Cell_it;


void usage(const char* const progName) {
  std::cout
    << " DESCRIPTION:\n"
    << "     " << progName << " is tool to compute a filtration of alpha complex from point cloud.\n"
    << "\n"
    << " USAGE:\n"
    << "      sh " << progName << " [-vh] <points file> <out file>\n"
    << "\n"
    << " OPTIONS:\n"
    << "     -v\n"
    << "         print " << progName << " version.\n"
    << "     -h\n"
    << "          print this.\n"
    << "\n"
    << " EXAMPLES:\n"
    << "     sh" << progName << " pointsFile outFile >out.log 2>err.log\n"
    << "\n"
    << " AUTHOR:\n"
    << "     written by Yuichi Motai.\n"
    << std::endl;
}


void version() {
  std::cout << "version 1.0" << std::endl;
}


class Simplex {
  friend std::ostream& operator<<(std::ostream& strm, const Simplex& s);
  
private:
  std::vector<Point> points_;
  double alpha_;
  
public:
  Simplex(const Vertex& v) {
    points_.push_back(v.point());
    alpha_ = 0;
  }
  
  Simplex(const Edge& e) {
    points_.push_back(e.first->vertex(e.second)->point());
    points_.push_back(e.first->vertex(e.third)->point());
    alpha_ = sqrt(CGAL::to_double
                  (CGAL::squared_radius(points_[0], points_[1])));
  }
  
  Simplex(const Facet& f) {
    for(auto i=0; i<4; i++)
      if(i != f.second)
        points_.push_back(f.first->vertex(i)->point());
    alpha_ = sqrt(CGAL::to_double
                  (CGAL::squared_radius(points_[0], points_[1], points_[2])));
  }
  
  Simplex(const Cell& c) {
    for(auto i=0; i<4; i++)
      points_.push_back(c.vertex(i)->point());
    alpha_ = sqrt(CGAL::to_double
                  (CGAL::squared_radius(points_[0], points_[1], points_[2], points_[3])));
  }

  double alpha() const {
    return alpha_;
  }

  ulong dim() const {
    return points_.size();
  }
  
  bool operator<(const Simplex& right) const {
    return alpha_ == right.alpha()
      ? points_.size() < right.dim() : alpha_ < right.alpha();
  }
};


std::ostream& operator<<(std::ostream& strm, const Simplex& s) {
  for(const auto& p: s.points_)
    strm << p[0] << '\t' << p[1] << '\t' << p[2] << '\t';
  return strm << s.alpha();
}

  
class Filtration {
private:
  std::vector<Simplex> simplices_;
  
public:
  Filtration(const Dt3D& dt) {
    // list up simplices with alpha values from the Delaunay diagram
    for(Cell_it it=dt.finite_cells_begin(); it!=dt.finite_cells_end(); ++it)
      simplices_.push_back(Simplex(*it));
    
    for(Facet_it it=dt.finite_facets_begin(); it!=dt.finite_facets_end(); ++it)
      simplices_.push_back(Simplex(*it));
    
    for(Edge_it it=dt.finite_edges_begin(); it!=dt.finite_edges_end(); ++it)
      simplices_.push_back(Simplex(*it));
    
    for(Vertex_it it=dt.finite_vertices_begin(); it!=dt.finite_vertices_end(); ++it)
      simplices_.push_back(Simplex(*it));
    
    // make filtration from these simplices
    std::sort(simplices_.begin(), simplices_.end());
  }

  std::vector<Simplex>::iterator begin() {
    return simplices_.begin();
  }

  std::vector<Simplex>::iterator end() {
    return simplices_.end();
  }
};




int main(int argc, char** argv) {
  // parsing arguments
  auto progName = argv[0];
  auto opt = 0;
  while((opt = getopt(argc,argv,"hv")) != -1){
    switch(opt){     
    case 'h':
      usage(progName);
      exit(0);
      
    case 'v':
      version();
      exit(0);
      
    case ':':
      std::cerr << "ERROR: " << opt << " needs value." << std::endl;
      usage(progName);
      exit(1);
      
    case '?':
      std::cerr << "ERROR: invalid option" << std::endl;
      usage(progName);
      exit(1);
    }
  }

  if(argc - optind != 2) {
    std::cerr << "ERROR: need 2 argments, but "
              << argc - optind << " here.\n" << std::endl;
    usage(progName);
    exit(1);
  }
  auto pointsFile = argv[optind++];
  auto outFile = argv[optind++];
  
  // input points from pointsFile
  std::ifstream fin(pointsFile);
  std::vector<Point> points;
  Point p;
  while(fin >> p)
    points.push_back(p);

    // Delaunay trianglation
  Dt3D dt(points.begin(), points.end());

  // get filtration from the Delaunay diagram
  Filtration filtration(dt);

  // output filtration
  std::ofstream fout(outFile);
  int i=0;
  for(auto it=filtration.begin(); it!=filtration.end(); ++it) 
    fout << *it << "\n";
      
  return 0;
}
