import std.stdio, std.conv, std.string, std.algorithm, std.array;


version(unittest){}
 else{
   void main(string[] args)
   {
     /* p := point
      * filt := filtration
      * ch := convex hull
      */
     auto filtFile = args[1];
     auto pOnCHFile = args[2];
     auto outFile = args[3];

     auto filt = readFilt(filtFile);
     auto pOnCH = readPOnCH(pOnCHFile);
     auto modifiedFilt = filtValToZero(filt, pOnCH);

     auto fout = File(outFile, "w");
     foreach(f; filt) {
       foreach(p; f.points) {
         fout.write(p.x, "\t", p.y, "\t", p.z, "\t");
       }
       fout.writeln(f.filtVal);
     }
   }
 }


Filter[] filtValToZero(Filter[] filt, Point[] pOnCH)
{
  auto modifiedFilt = filt.dup;
  foreach(ref f; modifiedFilt) {
    auto flag = true;
    
    foreach(p; f.points) {
      auto matchFlag = false;
      
      foreach(pch; pOnCH) {
        if(p.x == pch.x && p.y == pch.y && p.z == pch.z) {
          matchFlag = true;
          break;
        }
      }
      
      flag = flag && matchFlag;
      if(!flag)
        break;
    }

    if(flag)
      f.filtVal = 0;
  }
  
  return modifiedFilt;
}


Filter[] readFilt(string filename)
{
  Filter[] filt;

  foreach(line; File(filename, "r").byLine) {
    auto fields = line.to!string.strip.split("\t").map!(x => x.to!double);
    auto pNum = (fields.length - 1) / 3;
    Point[] points;
    
    foreach(i; 0..pNum) 
      points ~= new Point(fields[3 * i], fields[3 * i + 1], fields[3 * i + 2]);

    filt ~= new Filter(points, fields[$ - 1]);
  }

  return filt;
}


Point[] readPOnCH(string filename)
{
  Point[] pOnCH;
  foreach(line; File(filename, "r").byLine) {
    auto fields = line.to!string.strip.split("\t").map!(x => x.to!double);
    pOnCH ~= new Point(fields[0], fields[1], fields[2]);
  }
  return pOnCH;
}


class Point
{
  double x, y, z;
  this(double a, double b, double c) {
    x = a; y = b; z = c;
  }
}


class Filter
{
  Point[] points;
  double filtVal;

  this(Point[] p, double d) {
    points = p; filtVal = d;
  }

  @property ulong pNum() {return points.length;}
}
