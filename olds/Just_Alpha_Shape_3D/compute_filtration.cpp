#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Alpha_shape_3.h>

#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <iterator>
#include <typeinfo>
#include <cxxabi.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Gt;
typedef CGAL::Alpha_shape_vertex_base_3<Gt>          Vb;
typedef CGAL::Alpha_shape_cell_base_3<Gt>            Fb;
typedef CGAL::Triangulation_data_structure_3<Vb,Fb>  Tds;
typedef CGAL::Delaunay_triangulation_3<Gt,Tds>       Triangulation_3;
typedef CGAL::Alpha_shape_3<Triangulation_3>         Alpha_shape_3;
typedef Gt::Point_3                                  Point;
typedef Alpha_shape_3::Alpha_iterator               Alpha_iterator;
typedef Triangulation_3 Dt;
typedef typename Dt::Cell_handle Cell_handle;
typedef typename Dt::Vertex_handle Vertex_handle;
typedef typename Dt::Facet Facet;
typedef typename Dt::Edge Edge;


int main(int argc, char** argv)
{
  if(argc != 3) {
    std::cout << "Usage: " << argv[0] << " <points file> <out file>" << std::endl;
    return 1;
  }

  char* pointsFile = argv[1];
  char* outFile = argv[2];  

  // input points
  std::ifstream fin(pointsFile);
  std::list<Point> points;
  Point p;
  while(fin >> p)
    points.push_back(p);

  // compute alpha shape
  std::cout << "computing an alpha shape in GENERAL mode" << std::endl;
  Alpha_shape_3 as(points.begin(), points.end(), 0, Alpha_shape_3::GENERAL); 
  
  // compute filtration with alpha value
  std::cout << "computing the filtration with alpha value" << std::endl;
  std::list<CGAL::Object> objects;
  std::list<Gt::FT> alphas;
  
  as.filtration_with_alpha_values(CGAL::Dispatch_output_iterator<
                                  CGAL::cpp11::tuple<CGAL::Object, Gt::FT>,
                                  CGAL::cpp11::tuple<
                                  std::back_insert_iterator<std::list<CGAL::Object> >,
                                  std::back_insert_iterator<std::list<Gt::FT> > > >
                                  (std::back_inserter(objects),
                                   std::back_inserter(alphas)));
  
  // output filtration with alpha value
  std::cout << "outputting the filtration with alpha value" << std::endl;
  std::ofstream fout(outFile);
  std::list<CGAL::Object>::iterator objIt = objects.begin();
  std::list<Gt::FT>::iterator alphaIt = alphas.begin();

  while(objIt != objects.end() && alphaIt != alphas.end()) {
    if(const Vertex_handle* vhPtr = CGAL::object_cast<Vertex_handle>(&*objIt)) {
      fout << CGAL::sqrt(*alphaIt) << '\t' << **vhPtr << '\n';
      
    } else if(const Edge* ePtr = CGAL::object_cast<Edge>(&*objIt)) {
      Cell_handle ch = (*ePtr).first;
      int vID1 = (*ePtr).second;
      int vID2 = (*ePtr).third;
      fout << CGAL::sqrt(*alphaIt) << '\t'
           << *((*ch).vertex(vID1)) << '\t'
           << *((*ch).vertex(vID2)) << '\n';
      
    } else if(const Facet* fPtr = CGAL::object_cast<Facet>(&*objIt)) {
      Cell_handle ch = (*fPtr).first;
      int vID = (*fPtr).second;
      fout << CGAL::sqrt(*alphaIt) << '\t';
      int count = 0;
      for(int i=0; i<=3; i++) {
        if(i != vID) {
          fout << *((*ch).vertex(i));
          if(count < 2) {
            fout << '\t';
          }
          count++;
        }
      }
      fout << '\n';
      
    } else if(const Cell_handle* chPtr = CGAL::object_cast<Cell_handle>(&*objIt)) {
      fout << CGAL::sqrt(*alphaIt) << '\t';
      for(int i=0; i<=3; i++) {
        fout << *((**chPtr).vertex(i));
        if(i < 3) {
          fout << '\t';
        }
      }
      fout << '\n';
      
    } else {
      int status = 0;
      std::cerr << "ERROR: unexpected type in CGAL::Object, "
                << abi::__cxa_demangle((*objIt).type().name(), 0, 0, &status )
                << std::endl;
      return 1;
    }
    
    
    ++objIt;
    ++alphaIt;
  }

  
  return 0;
}
